package dam.moisesarjona.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class U3T1InitialAppActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;

    private TextView tvDisplay;
    private Button buttonIncrease, buttonIncrease2, buttonDecrease, buttonDecrease2, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null)
            count = savedInstanceState.getInt(("COUNT"));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u3_t1_initial_app);

        setUI();
    }

    private void setUI(){
        tvDisplay =findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonIncrease2 = findViewById(R.id.buttonIncrease2);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonIncrease2.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonIncrease: count++; break;
            case R.id.buttonIncrease2: count = count +2; break;
            case R.id.buttonDecrease: count--; break;
            case R.id.buttonDecrease2: count = count - 2; break;
            case R.id.buttonReset: count=0; break;

        }
        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("COUNT",count);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("COUNT");
    }
}